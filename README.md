# notification-producer

It creates and sends a notification to the server.

# Ecosystem
- [notification-server](https://gitlab.com/KKlochko/notification-server)
- [notification-producer](https://gitlab.com/KKlochko/notification-producer) (current)
- [notification-client](https://gitlab.com/KKlochko/notification-client)

# Setup

- Setup [notification-server](https://gitlab.com/KKlochko/notification-server), before continue.
- Clone this repository.
- Ensure that python and pip are installed:
  ```
  python --version
  python -m pip -V
  # or
  python3 --version
  python3 -m pip -V
  ```

- Create a virtual environment if needed:
  Using `virtualenv`:
  ```
  virtualenv .venv
  . .venv/bin/activate
  # or
  virtualenv3 .venv
  . .venv/bin/activate
  ```

  Using `poetry`:
  ```
  poetry shell
  ```

- Install the dependencies:
  Using `pip`:
  ```
  pip install -r requirements.txt
  # or
  pip3 install -r requirements.txt
  ```

  Using `poetry`:
  ```
  poetry install
  ```

- Run the main file:
  Use options ip and port to change the server address. 127.0.0.1:5554 is the default address.
  ```
  python -m notification_producer.main --help
  # or
  python3 -m notification_producer.main --help

  ```

# Commands

- Notify in the interactive mode:
  ```
  python -m notification_producer.main up
  # or
  python3 -m notification_producer.main up
  ```

  - Exit:
    - Enter the empty `title` and the empty `message`.
    - `Ctlr + C` (`C-c`).

- Notify once using text:
  ```
  python -m notification_producer.main notify message --message hello
  python -m notification_producer.main notify message --title greeting --message hello
  # or
  python3 -m notification_producer.main notify message --message hello
  python3 -m notification_producer.main notify message --title greeting --message hello
  ```

- Notify once using a file content:
  ```
  python -m notification_producer.main notify file README.md
  python -m notification_producer.main notify file --title file_notification README.md
  # or
  python3 -m notification_producer.main notify file README.md
  python3 -m notification_producer.main notify file --title file_notification README.md
  ```

# Author

Kostiantyn Klochko (c) 2023

# License

Under the GNU Affero General Public License v3.0 or later.

